//
//  ProductKind.swift
//  (C)Copyright: B.RF Group, 2024
//
import Foundation

enum ProductKind: String, CaseIterable
{
    case iphone = "iPhone"
    case ipad = "iPad"
    case imac = "iMac"
    case macbook = "MacBook"
    case accessory = "Аксессуары"
}
