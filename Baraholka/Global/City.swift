//
//  City.swift
//  (C)Copyright: B.RF Group, 2024
//
import Foundation

enum City: String, CaseIterable
{
    case moscow = "Москва"
    case stPeter = "Санкт Петербург"
    case kazan = "Казань"
    case nNovgorod = "Нижний Новгород"
    case tumen = "Тюмень"
    case omsk = "Омск"
    case rostor = "Ростов на Дону"
    case krasnoyarsk = "Красноярск"
    case novosibirsk = "Новосибирск"
    case ryazan = "Рязань"
}
