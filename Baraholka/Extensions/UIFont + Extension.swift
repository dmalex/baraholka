//
//  UIFont + Extension.swift
//  (C)Copyright: B.RF Group, 2024
//
import Foundation
import UIKit

enum CeraProStyle
{
    case normal
    case bold
}

extension UIFont
{
    static func ceraPro(style: CeraProStyle, size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: .regular)
    }
}
